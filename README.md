# Configuration de Home Assistant

## Mémo Floorplan

Plan 3D effectué sur Sweet Home 3D avec le plugin PhotoVideoRendering.

### Configuration export

* Size: 4k
* Appliquer les proportions: Vue 3D
* qualité : 4
* Heure: 03:00 (sauf une en pleine journée)
* Objectif: Defaut
* Ajouter les plafoniers: NON
* Moteur de rendu: YafaRay
* Aciter l'éclairage du ciel: NON (oui pour l'image tout éclairé uniquement)

### Exports

Il faut exporter les images ci-dessous :

* De jour
* Tout éclairé (tout 50% sauf toilettes et couloir 25%, entrée et sdb 35%)
* Nuit complet (tout à 5% sauf toilettes et couloir 3%)
* Nuit complet sauf salon (couloir 3%, entrée 5%, salon 50%)
* Nuit complet sauf couloir (couloir 25%, entrée 5%, salon 5%)
* Nuit complet sauf entrée (couloir 3%, entrée 35%, salon 5%)
* Nuit complet sauf entrée+couloir (couloir 25%, entrée 35%, salon 5%)
* Nuit complet sauf salon+couloir (couloir 25%, entrée 5%, salon 50%)
* Nuit complet sauf salon+entrée (couloir 3%, entrée 35%, salon 50%)

Ensuite, sur photoshop/gimp créer un overlay par pièce éclairée (tout le reste éteint) :

* salon-entrée-couloir - couloir éclairé
* salon-entrée-couloir - entrée éclairée
* salon-entrée-couloir - salon éclairé
* Toilettes
* Cuisine
* Bureau
* Chambre
* Salle de Bain